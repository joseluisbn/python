#Entrada de datos

mail = input("Introduce tu dirección de email")

print("Tu dirección de correo es: " + mail)

"""
Los datos almacenados por el método input son strings.
Para indicarle al programa que la variable a introducir es un número.
"""

edad = int(input("¿Cuántos años tienes?"))

print(edad)
